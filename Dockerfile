# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/corporate-reference.jar /corporate-reference.jar
# run application with this command line[
CMD ["java", "-jar", "/corporate-reference.jar"]
